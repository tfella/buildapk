<!--
    SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
    SPDX-License-Identifier: CC0-1.0
-->

# BuildApk

This is a wrapper around our Craft-based build system for android, intended to quickly generate and install apks from local, modified sources.

## Usage

1. Adjust the project name to the project you want to build
2. Adjust paths, if necessary
3. Adjust the apk name, if necessary
4. Execute script
