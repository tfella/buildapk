#!/usr/bin/python

# SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
# SPDX-License-Identifier: BSD-3-Clause

import os
import subprocess
import shutil

project = "neochat"

container = "invent-registry.kde.org/sysadmin/ci-images/android-qt515"
local_path = os.path.expanduser("~/kde/buildapk")
source_path = os.path.expanduser("~/kde/src")
android_sdk = os.path.expanduser("~/Android/Sdk")
apk_path = f"{local_path}/CraftRoot/tmp/{project}-app-arm64-v8a.apk"

craft = "/home/user/CraftRoot/craft/bin/craft.py"

if not os.path.exists(local_path):
    os.mkdir(local_path)
    os.system(f"chmod 777 -R {local_path}")

if not os.path.exists(local_path + "/CraftRoot"):
    os.mkdir(local_path + "/CraftRoot")
    os.system(f"chmod 777 -R {local_path}/CraftRoot")

if not os.path.exists(local_path + "/tmp"):
    os.mkdir(local_path + "/tmp")
    os.system(f"chmod 777 -R {local_path}/tmp")

def exec_in_docker(command: [str], capture=False, work_dir="") -> str:
    return subprocess.run(["docker", "run"] + ([] if work_dir == "" else ["--workdir", work_dir]) + ["-it" , "-v", f"{local_path}/CraftRoot:/home/user/CraftRoot", "-v", f"{local_path}/tmp:/home/user/tmp", "-v", f"{source_path}:/home/user/source", container] + command, capture_output=capture, text=True)

if not os.path.exists(local_path + "/CraftRoot/craft"):
    exec_in_docker(["curl", "https://raw.githubusercontent.com/KDE/craft/master/setup/CraftBootstrap.py", "--output", "tmp/CraftBootstrap.py"])
    exec_in_docker(["python", "tmp/CraftBootstrap.py", "--use-defaults", "--prefix", "/home/user/CraftRoot"])

exec_in_docker([craft, "--set", "version=master", project])
exec_in_docker([craft, project])
project_source_craft = exec_in_docker([craft, "-q", "--ci-mode", "--get", "sourceDir()", project], capture=True).stdout.strip()

exec_in_docker(["rm", project_source_craft, "-rf"])
exec_in_docker(["ln", "-s", f"/home/user/source/{project}", project_source_craft])

project_build = exec_in_docker([craft, "-q", "--ci-mode", "--get", "buildDir()", project], capture=True).stdout.strip()

exec_in_docker(["ninja", "install"], False, project_build)
exec_in_docker(["sh", "-c", f"git config --global --add safe.directory /home/user/source/{project} && {craft} --package {project}"], False, project_build)

if not os.path.exists(local_path + "/key.keystore"):
    os.system("keytool -genkey -noprompt -keystore {}/key.keystore -keypass 123456  -dname \"CN=None, OU=None, O=None, L=None, S=None, C=XY\" -alias mykey -keyalg RSA -keysize 2048 -validity 10000 -storepass 123456".format(local_path))
shutil.copy(f"{apk_path}, f"{local_path}/{project}.apk")
subprocess.run([android_sdk + "/build-tools/34.0.0/apksigner", "sign", "--ks", local_path + "/key.keystore", "--ks-pass", "pass:123456", f"{local_path}/{project}.apk"])
subprocess.run(["adb", "uninstall", "org.kde.neochat"])
subprocess.run(["adb", "install", f"{local_path}/{project}.apk"])
